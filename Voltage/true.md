When is it appropriate to use SST over FPE?
`SST is used for PCI scope reduction use cases`

Which of the following are advantages to Voltage SecureMail's Identity-Based Encryption (IBE) and Stateless Architecture?( Choose 3)
`Simple user experience across desktop, web, and mobile. 
`Lower cost of operations, and less infrastructure
`Seamlessly integrates with email and enterprise ecosystem

What is the default expiration time for large attachments?
`2 Weeks`

Which of the following is not a benefit of the SecureData key management system versus a "key vault"?
`All keys are stored in a key database for lookup

Messages that are encrypted with SecureMail cannot be forwarded to other users inside or outside of the enterprise.
`False`

Customers using Voltage SecureMail to migrate from on-premise email to Office365 should expect the following?
`No loss of data in the migration.

Voltage Identity-Based Encryption public key uses the sender's email address to create asymmetric keys in S/MIME message format with an IBE wrapper?
`True`

Why do customers decide to deploy a Hardware Security Module (HSM) with Voltage SecureData?
`To increase security when storing the base key used by the KDF`

Voltage SecureMail is not compatible with 3rd party DLP solutions
`False`

Select all of the following supported configurations for a SecureMail Appliance.( Choose 3)
`A dedicated Voltage SecureMail Server.
``"A standalone server that performs the functions of both the Voltage SecureMail Server and Management Console."
`A dedicated Voltage SecureMail Management Console.

For a standard demo of Voltage SecureData, Hadoop should be the first item to address.
`False`

True or False, When a competing product performs disk level encryption, the data is encrypted while in use by application.
`False`

What are the advantages of having FPE standardized?
`Proves the strength of the algorithm behind FPE`

What types of protection does "data centric" security provide? Select all that apply.
`Data in motion`
`Data in use`
`Data at rest`

The REST API tool demonstrates which feature of SecureData?
`Web Services`

Which of the following are benefits of Voltage SecureMail? (Choose 4)
`Single Voltage Indentity-Based Encryption solution fits all use cases.`
`Stateless Key Management Architecture`
`As simple as using regular email`
`Integrates with Exchange, DLP, AV/AS, and other enterprise solutions`

A digital signature is shown with messages in SecureMail proving the identity of the sender.
`True`

SecureData has native support for z/OS and HPE NonStop
`True`

True or false, the Voltage Indentity-Based Encryption solution led to the creation of IEEE 1363.3 – "Standard for Identity-Based Cryptographic Techniques using Pairings"?
`True`

True or False: SecureData provides native tools for Hadoop platforms.
`True`

Which encryption technology or technique did Adi Shamir use as inspiration for the idea of Identity based encryption in 1984? 
`Private Key Infrastructure (PKI)`

Demo appliances tend to be "less crowded" than a typical production SecureData appliance. You should indicate to the potential customer that their production environment will have more Districts and Identities. 
`False`

What standardization group looks over the FF1 mode of encryption?
`NIST`

SST Store derived encryption keys in the mapping table.. True or False
`True`

Using the Outlook client for SecureMail, users can open encrypted messages natively within outlook instead of using the ZDM.
`True`

The SecureData File Processor requires showing the customer a custom script before a demo.
`False`

How is "data centric" encryption different from Oracle TDE (Transparent Data Encryption)?
`Oracle TDE is a column/table level at-rest encryption solution, "Data Centric" encryption is a field level encryption solution.`

Which technology does SecureData SST use to replace a "Key Vault"?
`Token Mapping Table`

Which of the following is not an authentication option with SecureMail?
`Google Account`

Select the two goals of Voltage SecureMail Mobile?( Choose 2)
`PCI compliance and protection of customer data.`
`Protect PCI & PII data collected via mobile order taking.`

SecureMail can send large, encrypted attachments using SecureMail Large File delivery
`True`

With the SecureMail Data Centric solution, emails are encrypted at the gateway for messages sent to a 3rd party
`False`

Which types of sensitive fields can FPE protect?
`All of the above`

How do administrators of Voltage SecureData manage policies in the system?
`Through the management console`

You can demo an Active Directory integration using the Java Test Client
`True`

SecureMail is only compatible with Windows operating systems, especially when using the ZDM client.
`False`

Alice sends and encrypted email to Bob my encrypting the message with his private key.
`False`

The SecureMail client in outlook includes which convenience features?
`Automatic mail encryption`
`Send Secure button`

Which of the following is not a problem that SecureMail solves?
`Securing data at the field level within a database`

Keys used in SecureData must be stored for the life of the encrypted data.
`False`

When using the ZDM, there is no message in the users outbox unless the 'Copy Me' box is checked.
`True`

Which tool allows end users to open encrypted mail using a cloud based email providers like Gmail or Yahoo mail directly in the browser?
`ZDM`

Voltage SecureMail supports Multi-Tenant Architecture?
`True`

After what size is a file attachment considered a large attachment?
`Configurable`

Voltage REST API & SAOP API Clients store the derived key from SecureData master key locally on the client side.
`False`
