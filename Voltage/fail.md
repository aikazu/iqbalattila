What is the SimpleAPI function to encrypt data?
`Protect`

As an estimate, how many different data protection formats would a very complex SecureData environment have? Think our largest telecom customer as an example, whom has 1000s of databases and applications.
`400 to 600`

The AntiFishing feature in SecureMail provides end users with a _________ which is unique to the user and will always be the same.
`Picture`

When should a customer decide to route mail through the SecureMail Gateway? 1) When the Large File Attachment feature is configured for automatic storage and delivery 2) When using rule based encryption on messages (E.g. encrypt all messages with 'Confidential:' in subject line) 3) To enable use of the ZDM
`1,2,3`

SST manages the tokens in a unique way. What is the technology behind tokens in SST?
`AES Encryption`

What happens when a user loses their private key?
`The user needs to create a new key/cert pair.`

Which of the following are advantages of using tokenization technology such as SST?
`A token mapping table is used by SST.`
`A token vault is used to store all tokens used by the solution.`

Which tasks does the SecureData key management system perform? Select all that apply
`Authenticate key requests using LDAP (Lightweight Directory Access Protocol)``
`Provide keys using a Key Derivation Function (KDF)``
`Automatically rollover FPE keys`
`Use multiple authentication methods`

SecureData is an encryption platform that solves the following use cases
`PCI/ Compliance / Scope Reduction
`Email protection
`Collaboration Security
`Data de-identification and privacy

A customer would like a demo of SecureData to understand basic concepts of format preserving encryption for a variety of use cases. In this typical, early demo situation, which SecureData products should be shown to the customer?
`Web Services API (SOAP/REST)
`Management Console
`Simple API

Which of the following is not a supported client?
`SecureData Simple API for Python`

In which form is the Voltage SecureData appliance provided?
`Integrated ISO image`

In what situation does SecureMail store email attachments?
`When using the Large File Attachment feature

Which of the following are categories for various encryption formats? Select all that apply.
`United States Social Security Number
`Credit Cards
`Images
`Variable Length Strings

Compared to traditional PKI, the cost of operation is lower by this percentage:
`60-80%
`10-15%
`50-60%

